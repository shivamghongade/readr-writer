#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define BUFFER_SIZE 20

typedef struct {
    int buff[BUFFER_SIZE];
    sem_t full, empty;
    pthread_mutex_t mutex;
} shared;

shared sh;
int in = 0, out = 0;

void *producer(void *arg) {
    int item = 0;
    while (1) {
        sem_wait(&sh.empty);
        pthread_mutex_lock(&sh.mutex);
        sh.buff[in] = item;
        printf("Producer item: %d\n", item);
        in = (in + 1) % BUFFER_SIZE;
        pthread_mutex_unlock(&sh.mutex);
        sem_post(&sh.full);
        sleep(2);
        item++;
    }
    return NULL;
}

void *consumer(void *arg) {
    int item;
    while (1) {
        sem_wait(&sh.full);
        pthread_mutex_lock(&sh.mutex);
        item = sh.buff[out];
        printf("Consumer item: %d\n", item);
        out = (out + 1) % BUFFER_SIZE;
        pthread_mutex_unlock(&sh.mutex);
        sem_post(&sh.empty);
        sleep(2);
    }
    return NULL;
}

int main() {
    pthread_t ptid, ctid;
    sem_init(&sh.empty, 0, BUFFER_SIZE);
    sem_init(&sh.full, 0, 0);
    pthread_mutex_init(&sh.mutex, NULL);
    pthread_create(&ptid, NULL, producer, NULL);
    pthread_create(&ctid, NULL, consumer, NULL);
    pthread_join(ptid, NULL);
    pthread_join(ctid, NULL);
    return 0;
}

